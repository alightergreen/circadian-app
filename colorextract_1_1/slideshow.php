
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Circadian Landscape</title>
<!-- 	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style> -->
</head>
<body>

<script src="better-simple-slideshow/js/better-simple-slideshow.min.js"></script>
<link href="better-simple-slideshow/css/simple-slideshow-styles.css" rel="stylesheet">
<script src="http://hammerjs.github.io/dist/hammer.min.js"></script>

<div id="wrap" class="bss-slides slideshow">
<?php
include_once("photoapp.inc.php");
$slideshowlist = selectphotos();
$curtime = selectphotos()[1];
$slideshowlist = selectphotos()[0];
$shortlist = array_slice($slideshowlist, 0, 6);



// while(true)
// {
//     $temptime = date("G");
// 	if ($temptime != $curtime) {
// 		$shortlist = selectphotos();
// 		$curtime = selectphotos()[1];
// 		$shortlist = selectphotos()[0];
// 		sleep(60);
// 	} else {
// 		sleep(60);
// 	}
// }

foreach ($shortlist as $sskey => $image) :
	$image = $shortlist[$sskey]['image'];
	$path = basename($image, ".jpg");

	echo 
	"<figure>
		<img src='$image' alt='test image' width='100%' />
		<figcaption>" . $path . "</figcaption>
	</figure>";
	// Log the photos selected and the time of day

	$sslogsql = "INSERT INTO slideshowlog (image) VALUES ('{$shortlist[$sskey]['image']}')";
	$sslogresult = dbconn($sslogsql )[1];
endforeach;
?>
</div>

<script>
var opts = {
            //auto-advancing slides? accepts boolean (true/false) or object
            auto : { 
                // speed to advance slides at. accepts number of milliseconds
                speed : 2500000, 
                // pause advancing on mouseover? accepts boolean
                pauseOnHover : false 
            },
            // // support swiping on touch devices? accepts boolean, requires hammer.js
            swipe : true 
        };


makeBSS('.slideshow', opts);
</script>

</body>
</html>




