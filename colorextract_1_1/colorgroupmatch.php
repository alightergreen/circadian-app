<?php
include_once("photoapp.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Color Group Match</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />

<!-- This file should probably also have some logic to see if the color group is set and skip that photo.
Or better yet a variable where you can choose to skip or rerun them all if you have improved the function somehow.
That way if you add three photos to a folder you don't have to rerun hundreds of color matches. -->

<?php

$imagesql = "SELECT color FROM MainPhotoColor";
$imagedata = dbconn($imagesql)[0];
// $imagedata = $imagedata[1];

$cgsql = "SELECT HexCode, ColorGroup, id FROM HexColorGroups";
$cgdata = dbconn($cgsql)[0];
$cgresult = dbconn($cgsql)[1];
// shuffle($cgdata);

// var_dump($imagedata);
// var_dump($cgdata);


foreach ($imagedata as $imagekey => $value) {
	$subresult = 300;
	$group = "Null";

	$splitcolor = str_split($value['color'], 2); 
	foreach ($splitcolor as $key => $colorcount) {
		$splitcolordec[$key] = hexdec($colorcount);
	}
	echo "<br><br>" . "split image colors as decimals:"; var_dump($splitcolordec); 

	$id = $imagekey+1;
	$imagecolorsql = "UPDATE MainPhotoColor SET redcount = '$splitcolordec[0]', greencount = '$splitcolordec[1]', bluecount = '$splitcolordec[2]' WHERE id = '$id'";
	dbconn($imagecolorsql);
	
	foreach ($cgdata as $cgkey => $option) {
		$tempabs = [];
		// var_dump($tempabs);
		$splitgroup = str_split($option['HexCode'], 2);

		foreach ($splitgroup as $key => $colorcount) {
			$splitgroupdec[$key] = hexdec($colorcount);
		}
		// echo "split group colors as decimals:";

		$gid = $cgkey+1;
		// $groupcolorsql = "UPDATE HexColorGroups SET RedCount = '$splitgroupdec[0]', GreenCount = '$splitgroupdec[1]', BlueCount = '$splitgroupdec[2]' WHERE id = '$gid'";
		// dbconn($groupcolorsql);


		// var_dump($splitcolor); echo "<br>" . "<br>";
		// var_dump($splitgroup); echo "<br>" . "<br>";
		$subtracted = array_map(function ($x, $y) { return $y-$x; } , $splitcolordec, $splitgroupdec);
		// var_dump($subtracted); echo "<br>" . "<br>";
		$tempresult = array_combine(array_keys($splitcolordec), $subtracted);
		// var_dump($tempresult);
		foreach ($tempresult as $nums) {
			$tempabs[] = abs($nums);
		}
		// var_dump($tempabs);
		$totaltemp = $tempabs[0] + $tempabs[1] + $tempabs[2];
		// var_dump($totaltemp); echo "<br>" . "<br>";
		if ($totaltemp == $subresult) {
			if ($cgkey == mysqli_num_rows($cgresult)) {
				echo mysqli_num_rows($cgresult);
				$group = "Oh No!";
			}
		}  
		if ($totaltemp < $subresult) {
			$subresult = $totaltemp;
			$group = $cgdata[$cgkey]['ColorGroup'];
		} 
	}
	echo $group. "<br>" .$subresult. "<br>" . $id ."," . $gid . "<br>";
	$updatesql = "UPDATE MainPhotoColor SET colorgroup = '$group' WHERE id = '$id'";
	dbconn($updatesql);
}


?>

</div>
</body>
</html>