<?php
include_once("photoapp.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Select Photos for Slideshow</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />

<!-- Select image results, group results by color group, add all % for a color group together, save results to another table as -> image, color group, %, rank [This rank is the rank of the color group out of all the color grups for that image].  -->

<?php
// Find the current time in G format.
$curtime = date("G");
// echo "Current Time:" . $curtime . "<br>";
//Connecting to DB. Selecting all colors
//Compare current time to hours table and get the current period label.
$periodsql = "SELECT Period FROM Hours WHERE Hour24 = '$curtime'";
$perioddata = dbconn($periodsql)[0];
$periodresult = dbconn($periodsql)[1];

		$rowcount = $periodresult->num_rows;
		if ($rowcount == 0) {
			echo "0 period results" . "<br>";
		}
		if ($rowcount > 1) {
			echo "too many period results" . $rowcount . "<br>";
		}
$period = $perioddata[0]['Period'];



//Compare to the Period Table. Find that period's color groups.
$cgperiodsql = "SELECT ColorGroup FROM PeriodColors WHERE Period = '$period'";
$cgpsdata = dbconn($cgperiodsql)[0];
$cgpsresult = dbconn($cgperiodsql)[1];

		$rowcount = $cgpsresult->num_rows;
		if ($rowcount == 0) {
			echo "0 period results" . "<br>";
		}

$group = $cgpsdata[0]['ColorGroup'];
echo "<br>"; 

//This part SHOULD grab photos from the ranked color groups table where color group matches the color group you need and rank equals 1 or 2.
		//add in cologroup and rank to be able to pass to the log later. 
$selectimagessql = "SELECT image FROM RankedColors WHERE colorgroup = '$group' AND rank = '1,2' ";
$imagerows = dbconn($selectimagessql)[0];
$matchingimages = dbconn($selectimagessql)[1];

$rowcount = $matchingimages->num_rows;
	if ($rowcount == 0) {
		echo "0 image results";
	}

// Suffle the resulting array of images. Display them to the screen.
$uniqueimages = array_map("unserialize", array_unique(array_map("serialize", $imagerows)));
// echo"<br>" . "Unique Image List:";
var_dump($uniqueimages);
// echo"<br>";
shuffle($uniqueimages);
$slideshowlist = 
	$uniqueimages;
// echo"<br>" . "Slideshow List:";

// $array = [($slideshowlist), ($curtime)];
// return $array;
?>

</div>
</body>
</html>
