<?php
include_once("photoapp.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Rank Top Colors by Image</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />

<?php
//Getting the possible directories for the upcoming form. Directories of images 
//must be in the colorextract_1_1 folder to make it in here. 
//Excludes system directories by excluding anything with a "."" in its name so don't name things stupid. 
	$directoryarr = array();
	$cwd = getcwd();
	$dirarr = scandir("$cwd");
	foreach ($dirarr as $possdir) 
	{	
		if (strpos($possdir, '.') === FALSE)
			{
				array_push($directoryarr, "$possdir");
			}
	}
	if (isset($directoryarr)) {
		// var_dump($directoryarr);
	}
?>

<form action="#" method="post" enctype="multipart/form-data">
	<label>Directory:</label><br>
<?php
//list of possible directories from above is passed in to make a radio button list here. 
//That means you can only select one directory at a time to run this on for now. 
		foreach ($directoryarr as $folder)
			{
			echo "<input type='radio' name='possfolder' value='$folder'> $folder <br>";
			}
?>
		</br>
<input type="submit" name="action" value="Process" />
</form>
<?php
if (empty($_POST)) {
	exit('Waiting on you!');
}

//First we set our variables with the results from the above form
			//echo ($_POST['possfolder']);


//Now we are looking in the folder we selected witht he radio button in the form and making a list
//of all the files in it. Files must be jpg, gif, or png to make it through the color extract function
//so we have limited it to only pulling those. Also note its matching on basic text and capitaliation so
//GIF and jpeg will probably not get picked up. Normalize file extensions manually before running this.

$exten = "jpg";
$files = dirfile($_POST['possfolder'], $exten);
			//var_dump ($files);
foreach ($files as $filekey => $value) :
	$filepullsql = "SELECT image, colorgroup, percent FROM MainPhotoColor WHERE image = '$value'";
	$filepulldata = dbconn($filepullsql)[0];
	$filepullresult = dbconn($filepullsql)[1];
		echo "search successful" . $filekey . "</br>";

	foreach ($filepulldata as $key => $single_image) :
		// var_dump($filepulldata);
		var_dump($single_image);
		$matchsql = "SELECT id, percent FROM RankedColors WHERE colorgroup = '{$single_image['colorgroup']}' AND image = '{$single_image['image']}'";
		$matchdata = dbconn($matchsql)[0];
		$matchresult = dbconn($matchsql)[1];
			echo "search successful";

		$empty = empty($matchdata);
		if ($empty == 1) {
			$insertsql = "INSERT INTO RankedColors (image,colorgroup,percent) VALUES ('{$single_image['image']}','{$single_image['colorgroup']}','{$single_image['percent']}')";
			$insertresult = dbconn($insertsql)[1];
				echo "new record created </br>";
			continue;
		}

		$rowcount = count($matchdata);
		if ($rowcount != 0) {
			$percentsum = $matchdata[0]['percent']+$single_image['percent'];
			$updatesql = "UPDATE RankedColors SET percent = '$percentsum' WHERE id = '{$matchdata[0]['id']}'";
			$updateresult = dbconn($updatesql)[1];
			echo "added to percent" . "<br>";
			continue;
		}

		echo "AHHH! EVERYTHING IS BROKWN!!!!";
	endforeach;

	$rankselectsql = "SELECT percent, id FROM RankedColors WHERE '{$single_image['image']}' = image";
	$unrankeddata = dbconn($rankselectsql)[0];
	$unrankedresult = dbconn($rankselectsql)[1];
		echo "unranked records retrived </br>";

	$sortedcolors = rsort($unrankeddata);

	$a=1;
	foreach ($unrankedresult as $unrankedkey => $unranked) {		
		$rankingsql = "UPDATE RankedColors SET rank = '$a' WHERE id = '{$unrankeddata[$unrankedkey]['id']}'";
		$rankedresult = dbconn($rankingsql)[1];
			echo "records ranked </br>";
		$a++;
	}
endforeach;

?>

</div>
</body>
</html>