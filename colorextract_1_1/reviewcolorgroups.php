<?php
include_once("photoapp.inc.php");
?>

<!-- This is where the web page starts. -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Review Assigned Color Groups</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />
<br />
<br />

<?php
$mainsql = "SELECT image, color, colorgroup FROM MainPhotoColor";
$maindata = dbconn($mainsql)[0];
$mainresult = dbconn($mainsql)[1];

// var_dump($maindata);

$a = 1;
$imagename = $maindata[0]['image'];
echo "<table> <tr><td>". $imagename . "</td><td></td><td></td></tr>";
foreach ($maindata as $row => $cycle) {
	if ($maindata[$row]['image'] == $imagename){
	echo "<tr><td style=\"background-color:#".$maindata[$row]['color'].";\"></td><td>".$maindata[$row]['color']."</td><td>" . $maindata[$row]['colorgroup']. "</td></tr>";
	} else {
		$imagename = $maindata[$row]['image'];
		echo "</table><br>
		<table> <tr><td>". $imagename . "</td><td></td><td></td></tr>";
		echo "<tr><td style=\"background-color:#".$maindata[$row]['color'].";\"></td><td>".$maindata[$row]['color']."</td><td>" . $maindata[$row]['colorgroup']. "</td></tr>";
	}
}
?>
</table>
<br />
</div>
</body>
</html>
