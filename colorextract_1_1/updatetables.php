<?php
include_once("photoapp.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Image Color Extraction</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />

<!-- This section pulls up all the colors from the main color table and compares them to the hex color table to see if they already have a color group set. If so it adds it to the database.  -->

<table>
	<tr><td>Color</td><td>Color Code</td><td>Color Group Name</td></tr>
	<?php
	//Connecting to DB. Selecting all colors.
		$conn = dbconn();
		$sql = "SELECT color FROM MainPhotoColor";

		if ($conn->query($sql) != TRUE) :
		    echo "Error with: " . $sql . "<br>" . $conn->error;
			continue;
		endif;

		$colors = $conn->query($sql);

		if ($colors->num_rows == 0) {
			echo "0 results";
		}

		
$a=1;
    // output data of each row of the resulting array. match to color group name in hex color table. display search results and visual results in on page table. 
    while($row = $colors->fetch_array()) { 
    	$hexcolor = $row["color"];
    	$groupsql = "SELECT ColorGroup FROM HexColorGroups WHERE '$hexcolor' = HexCode";

		if ($conn->query($groupsql) != TRUE) :
		    echo "Error with: " . $groupsql . "<br>" . $conn->error;
			continue;
		endif;

		$colorgroup = $conn->query($groupsql);

		if ($colorgroup->num_rows == 0) {
			echo "0 results" . "<br>";
		}
			echo "Colors Retrieved" . "</br>";

		$grouprow = $colorgroup->fetch_array();

		echo "<tr><td style=\"background-color:#". $row["color"] .";\"></td><td>".$row["color"] ."</td><td>" .$grouprow["ColorGroup"]. "</td></tr>";

		$insertcolorgroupsql = "UPDATE MainPhotoColor SET colorgroup = '$grouprow[ColorGroup]' WHERE color='$hexcolor'";
		if ($conn->query($insertcolorgroupsql) != TRUE) :
		    echo "Error with: " . $insertcolorgroupsql . "<br>" . $conn->error;
			continue;
		endif;
    }
    	echo "Color saved" . $a++;
		$conn->close();
	?>
</table>



</div>
</body>
</html>
