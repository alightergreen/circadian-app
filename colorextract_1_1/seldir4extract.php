<?php
include_once("colors.inc.php");
include_once("photoapp.inc.php");
$ex=new GetMostCommonColors();
?>

<!-- This is where the web page starts. -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Image Directory Select</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />
<br />
<br />
<?php
//Getting the possible directories for the upcoming form. Directories of images 
//must be in the colorextract_1_1 folder to make it in here. 
//Excludes system directories by excluding anything with a "."" in its name so don't name things stupid. 
	$directoryarr = array();
	$cwd = getcwd();
	$dirarr = scandir("$cwd");
	foreach ($dirarr as $possdir) 
	{	
		if (strpos($possdir, '.') === FALSE)
			{
				array_push($directoryarr, "$possdir");
			}
	}
	if (isset($directoryarr)) {
		// var_dump($directoryarr);
	}
?>

<!-- This is where the Form starts! -->

<form action="#" method="post" enctype="multipart/form-data">
	<label for="num_results">Number of colors:</label>
			<input type="text" name="num_results" id="num_results" value="20" />
		</br>
	<label for="delta">Delta:</label>
			<input type="text" name="delta" id="delta" value="24" /> (1-255)
		</br>
	<label>Reduce Brightness:</label>
			<input type="radio" name="reduce_brightness" checked="checked" value="1" />Yes
			<input type="radio" name="reduce_brightness"  value="0" />No
		</br>	
	<label>Reduce gradient:</label>
			<label><input type="radio" name="reduce_gradient" checked="checked" value="1" /> Yes</label>
			<label><input type="radio" name="reduce_gradient"  value="0" /> No</label>
		</br>	
	<label for="exten">Extensions:</label>
			<input type="text" name="exten" id="exten" value="jpg,png,gif" />
		</br>
	<label>Directory:</label><br>
<?php
//list of possible directories from above is passed in to make a radio button list here. 
//That means you can only select one directory at a time to run this on for now. 
		foreach ($directoryarr as $folder)
			{
			echo "<input type='radio' name='possfolder' value='$folder'> $folder <br>";
			}
?>
		</br>
<input type="submit" name="action" value="Process" />
</form>
<br />
<br />
<br />

<!--  Now we are finally getting to the color extract part -->

<pre>
<?php
if (empty($_POST)) {
	exit('Waiting on you!');
}

//First we set our variables with the results from the above form
			//echo ($_POST['possfolder']);
$delta = $_POST['delta'];
$reduce_brightness = $_POST['reduce_brightness'];
$reduce_gradients = $_POST['reduce_gradient'];
$num_results = $_POST['num_results'];
$exten = $_POST['exten'];
$c = 1;

//Now we are looking in the folder we selected witht he radio button in the form and making a list
//of all the files in it. Files must be jpg, gif, or png to make it through the color extract function
//so we have limited it to only pulling those. Also note its matching on basic text and capitaliation so
//GIF and jpeg will probably not get picked up. Normalize file extensions manually before running this.
$files = dirfile($_POST['possfolder'], $exten);
			//var_dump ($files);
foreach ($files as $filekey => $value) :
	$imagefilesql = "SELECT image FROM MainPhotoColor WHERE image='$value'";
	$imagefiledata = dbconn($imagefilesql)[0];
	$imagefileresult = dbconn($imagefilesql)[1];
	echo "search successful" . $filekey . "</br>";

	if ($imagefileresult->num_rows > 0)
	{
		echo "non-unique" . $c++ ."</br>";
		continue;
	}

	$colors=$ex->Get_Color("$value", $num_results, $reduce_brightness, $reduce_gradients, $delta);

	?>
	</pre>
	<table>
	<tr><td>Color</td><td>Color Code</td><td>Percentage</td><td rowspan="<?php echo (($num_results > 0)?($num_results+1):22500);?>"><img src= "<?php echo $value;?>" alt="test image" /></td></tr>
	<?php
	$a = 1;
	foreach ( $colors as $hex => $count ) :
		if ( $count > 0 ) :
			$insertcolorsql = "INSERT INTO MainPhotoColor (image, color, percent)
			VALUES ('$value', '$hex', '$count')";
			$insertcolorresult = dbconn($insertcolorsql)[1];
				echo "New record created successfully" . $a++ . "</br>";
			echo "<tr><td style=\"background-color:#".$hex.";\"></td><td>".$hex."</td><td>$count</td></tr>";
		endif;
	endforeach;
	?>
	</table>
	<br />
	<?php
endforeach;
?>

<!-- rewrite this log so it happens at the beginning and counts up one for each photo processed. -->
<?php
$logsql = "INSERT INTO log (delta, bright, gradient, numresults, exten, photosprocessed)
VALUES ('$_POST[delta]', '$_POST[reduce_brightness]', '$_POST[reduce_gradient]', '$_POST[num_results]', '$_POST[exten]', $c)";
$logresult = dbconn($logsql)[1]; 
   echo "New log record created successfully."
?>
</div>
</body>
</html>
