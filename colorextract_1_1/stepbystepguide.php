1. Open seldir4extract.php in browser. 
// This file stands for select directory for extract. 
// It allows you to set your preferences for how the color extractor works and, obviously,
// select the directory you will pull images from. 
// All directories must be int he color_extract_1_1 folder.

2. Set preferences and run. 
// This will run the color extractor and save the results. 
// It will check to see if a file has been run before and won't reextract it.
// Practically this means you can only run this file once ever on any image. 
// If you don't like the settings you picked you need to go to the database and delete all results for
// that image or just truncate the table and state over. 
// Your browser might crash after 30 photos or so. That's ok. Just refresh the page and resubmi the form.
// Since the photos it already processed are in the database and the code checks for that it won't reprocess
// the same photos and will start on the next 30 or so. This also means that you can just added photos to the
// folder as time goes on and run through these steps. You don't have to dump database or anything like that.
// Pros and cons. Pros and Cons.

3. Open colorgroupmatch.php in browser. 
// Opening this file will run it. 
// This file looks at the colors returned from the extractor and compares them to the colors in the ExColor Group table. 
// This table is a list of web accempted color with color groups set on them. These are set by the W3C I beleive, 
// so they are a standardized lace to start from when assigning groups to all the random colors returned from these images.
// You can run this as many times as you want but you SHOULD get the same results everytime. 

4. Review results.
// Either in the browser using the reviewcolorgroups.php file for a visual reference and the MySQL 
// MainPhotoColor table for a data reference to check out your color grouping results. 
// If ANY of your rows return Oh No! in the color group, you need to run manualcolorgroupassign.
// This file will help you assign color groups by hand so you can move on. 

5. Run topcolors.php
// This file will take you expanded results from the colorextractor and reduce them down by color group. 
// Then it will give them a rank in order from the color group that covers the highest percentage to the 
// color group that covers the lowest percentage for each photo. 

6. Run the slideshow.php
// This will use seldisplayphoto.php to get photos the have colors the coordinate with the time of day. 
// If you already had slideshow running it will update every 10 minutes so you don't need to refresh it. 
// it will pull the new images automatically. 

Other files:
handcommands.php is a file I used to manually update a couple tables when I needed to get a 
	column into a table and phpMyAdmin wasn't playing nice. 
manualassigncolorgroup.php is a way to add in a column from a CSV where you have manually set 
	the color group for all results returned from the extractor. Only used for testing pusposes.
index.php is the example file that came with this fre color extraction library.
colors.inc.php is the color extraction function. Check it out but probably don't. 
photoapp.inc.php is some basic commands that I moved into one place. 
updatetables.php was an attempt to get it to match the color group just off the 140 web colors, but the VAST
	majority of the colors returned were not in that table so no dice.


