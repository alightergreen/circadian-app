<?php
include_once("photoapp.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Image Color Extraction</title>
	<style type="text/css">
		* {margin: 0; padding: 0}
		body {text-align: center;}
		div#wrap {margin: 10px auto; text-align: left; position: relative; width: 500px;}
		img {width: 200px;}
		table {border: solid #000 1px; border-collapse: collapse;}
		td {border: solid #000 1px; padding: 2px 5px; white-space: nowrap;}
		br {width: 100%; height: 1px; clear: both; }
	</style>
</head>
<body>
<div id="wrap">
<br />

<!-- FINALLY! PHP: -->

<?php
	$a=1;
    $conn = dbconn();

    if (($handle = fopen("/Users/dfritsch/Dropbox/AHappyHome/colorextract_1_1/colorsbyhand.csv", "r")) !== FALSE)
    {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
        {
        	$sql = "UPDATE MainPhotoColor SET handcolorgroup='{$data[1]}' WHERE id = '{$data[0]}'";
            if ($conn->query($sql) != TRUE) :
			    echo "Error with: " . $sql . "<br>" . $conn->error;
				continue;
			endif;
			echo $a++ . "<br>";
        }
    fclose($handle);
    }
?>

</div>
</body>
</html>